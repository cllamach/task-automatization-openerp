{
'name':'Automated Tasks',
'version': '1.0',
'author':'Carlos Llamacho',
'category':'Tools',
'description': """
A module that creates cron jobs for 
a certain task of a certain project.
""",
'depends':['base_setup','project'],
'data':['AutomatedTask_view.xml'],
'installable':True,
}

