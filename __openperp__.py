# -*- coding: utf-8 -*-

{
    'name': 'Automated Task',
    'author': 'Carlos Llamacho',
    'version': '1.0'
    'description': """A module that creates cron jobs for 
    a certain task of a certain project."""
    'depends': ['base_setup', 'project'],
    'data': ['AutomatedTask_view.xml']
    'installable': True

}