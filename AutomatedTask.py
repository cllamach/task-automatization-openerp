# -*- coding: utf-8 -*-

"""
Module that creates automated task for determined project.

@author: Carlos Miguel Llamacho

@date: Tuesday Sep 3 2013 13:36

"""

from osv import osv, fields


class AutomatedTask(osv.osv):
    """Creates a cron job based on a selected task of a selected project. The
    settings for the cron job are entered by the user."""
    _name = 'project.task.auto'

    _columns = {
    'project_id': fields.many2one('project.project', 'Project', required=True),
    'task_id': fields.many2one('project.task', 'Task', required=True,
        help="The task that is going to be repeated."),
    'interval_type': fields.selection((('days', 'Days'),
                                       ('weeks','Weeks'),
                                       ('hours','Hours'),
                                       ('minutes','Minutes'),
                                       ('work_days','Work '
'days')), 'Interval type', 
                       help="Set this to the type of time that must pass" 
                       " between tasks."),
    'interval_number': fields.integer('Interval number', size=16, 
        help="Time between one call and the next one."),
    'numbercall': fields.float('Calls', digits=(16,2), 
        help="The number of times that the function is going to be called." 
        " -1 to put it negative."),
    'nextcall': fields.datetime('Next call', required=True, 
        help="Set the first time the task should be called."),
    'active': fields.boolean('Active', help="This shows if the job is running"
    "in the system.")}

    def create_ir_job(self, cr, uid, ids, context=None):
        """Creates the cron job to the function create_task.
        Settings like interval_number and numbercall are taken from the 
        fields."""
        ir_cron_obj = self.pool.get('ir.cron')
        automated_task = self.browse(cr, uid, ids, context=None)
        try:
            for task in automated_task:
                ir_cron_obj.create(cr, uid, {'function': 'create_task',
                    'interval_type': task.interval_type,
                    'user_id': uid,
                    'name': 'Automated ' + task.task_id.name,
                    'args': "({0}, {1})".format(str(task.task_id.id),
                        str(task.project_id.id)),
                    'numbercall': task.numbercall,
                    'nextcall': task.nextcall,
                    'priority': 5,
                    'doall': True,
                    'active': True,
                    'interval_number': task.interval_number,
                    'model': 'project.task.auto'}, context=None)
            self.write(cr, uid, ids, {'active': True}, context=None)
            return 0
        except IndexError, error:
            return 1

    def delete_ir_job(self, cr, uid, ids, context=None):
        """Searches among the ir jobs for one with the name as the same record
        in project.task.auto and deletes it."""
        auto_tasks = self.browse(cr, uid, ids, context=None)
        if len(auto_tasks) == 1:
            ir_cron_obj = self.pool.get('ir.cron')
            cron_job_ids = ir_cron_obj.search(cr, uid, [('name','=',
                'Automated '+auto_tasks[0].task_id.name)], context=None)
            ir_cron_obj.unlink(cr, uid, cron_job_ids, context=None)
            self.write(cr, uid, ids, {'active': False}, context=None)
        else:
            raise osv.except_osv('Error', 'More than one record was returned.')

    def create_task(self, cr, uid, task_id, project_id, context=None,):
        """Creates a new task using the information from the
        selected task and project."""
        #Arguments passed to via the cron job.
        #Are the task object and the project object
        task_id = task_id
        project_id = project_id

        #This holds the model for tasks.
        project_task_obj = self.pool.get('project.task')
        task =  project_task_obj.browse(cr, uid, task_id, context=None)
        #Create a new task record using the data from the task that the cron
        #job passes to the function.
        project_task_obj.create(cr, uid, {'name': task.name,
            'description': task.description,
            'user_id': task.user_id.id,
            'project_id': project_id,
            'active': True,
            'date_start': task.date_start,
            'sequence': task.sequence,
            'date_end': task.date_end,
            'planned_hours': task.planned_hours,
            'date_deadline': task.date_deadline,
            'company_id': task.company_id.id,
            'priority': task.priority, 'state':
            'draft', 'progress': 0.00,
            'stage_id':task.stage_id.id})

    def onchange_task(self, cr, uid, ids, task, context=None):
        """Method that picks the project_id of the task selected and pass_it to
        the field project_id"""
        tasks_obj = self.pool.get('project.task')
        tasks = tasks_obj.browse(cr, uid, task, context=None)
        return {'value': {'project_id':tasks.project_id.id}}

AutomatedTask()
